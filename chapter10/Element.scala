import Element._

abstract class Element
{
  def contents: Array[String]

  def height: Int = contents.length
  def width: Int = if(height == 0) 0 else contents(0).length

  def above(that: Element): Element =
  {
    val this1 = this widen that.width
    val that1 = that widen this.width
    new ArrayElement(this1.contents ++ that1.contents)
  }

  def beside(that: Element): Element =
  {
    // OO way
    //val contents = new Array[String](this.contents.length)
    //for(i <- 0 until this.contents.length)
    //{
    //  contents(i) = this.contents(i) + that.contents(i)
    //}
    //new ArrayElement(contents)

    // FP Way
    val this1 = this heighten that.height
    val that1 = that heighten this.height
    new ArrayElement(
      for(
        (line1, line2) <- this1.contents zip that1.contents
      ) yield line1 + line2
    )
  }

  def widen(w: Int):Element =
  {
    if (w <= width)
    {
      this
    }
    else
    {
      val left = elem(' ', (w - width) / 2, height)
      val right = elem(' ', w - width - left.width, height)
      left beside this beside right
    }
  }

  def heighten(h: Int): Element =
  {
    if(h <= height)
    {
      this
    }
    else
    {
      val top = elem(' ', width, (h - height) / 2)
      val down = elem(' ', width, h - height - top.height)
      top above this above down
    }
  }

  override def toString = contents mkString "\n"
}

object Element
{
  private class ArrayElement(val contents: Array[String]) extends Element
  {
    //override def contents = conts
  }

  private class LineElement(s: String) extends Element
  {
    val contents = Array(s)
    override def width = s.length
    override def height = 1
  }

  private class UniformElement(c: Char, override val width:Int , override val height:Int ) extends Element
  {
    private val line = c.toString * width
    def contents = Array.fill(height)(line)
  }

  def elem(contents: Array[String]):Element =
  {
    new ArrayElement(contents)
  }

  def elem(c: Char, width: Int, height: Int):Element =
  {
    new UniformElement(c, width, height)
  }

  def elem(line: String): Element =
  {
    new LineElement(line)
  }
}
