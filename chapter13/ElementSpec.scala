import org.scalatest.WordSpec
import org.scalatest.prop.Checkers

class ElementSpec extends WordSpec with Checkers
{
  "elem result" must {
    "have passed width" in {
      check((w: Int) => w > 0 ==> (Element.elem('x', w, 3).width == w))
    }
  }
}
