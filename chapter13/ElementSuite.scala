import org.scalatest.{Matchers, FlatSpec}

class ElementSuite extends FlatSpec with Matchers
{

  "An element" should "have same width as declared" in
  {
    val ele = Element.elem('x', 2, 3)
    ele.width should be (2)
  }
}
