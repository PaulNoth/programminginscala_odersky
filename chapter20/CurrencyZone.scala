abstract class CurrencyZone {
  type Currency <: AbstractCurrency

  def make(amount: Long): Currency

  val CurrencyUnit: Currency

  abstract class AbstractCurrency {
    val amount: Long
    def designation: String

    def + (that: Currency): Currency = {
      make(this.amount + that.amount)
    }

    def * (x: Double): Currency = {
      make((this.amount * x).toLong)
    }

    override def toString: String =
      ((amount.toDouble / CurrencyUnit.amount.toDouble)
      formatted ("%."+ decimals(CurrencyUnit.amount) +"f")
      +" "+ designation)

    private def decimals(n: Long): Int =
      if (n == 1) 0 else 1 + decimals(n / 10)
  }
}
